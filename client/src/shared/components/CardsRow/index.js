import React from "react";
import ChatQuantum from "../../../components/ChatQuantum";
import Fotones from "../../../components/Fotones";
import Receptor from "../../../components/Receptor";
import Siftting from "../../../components/Siftting";

import pol1 from "../../img/polarizado_lente.gif";
import pol2 from "../../img/polarizado_lente_2.gif";

const CardsRow = (props) => (
      <div className="container">
        <div className="row ">

          <div className="col s12">
            <div className="card">
              <div className="card-content">
                <div className="row" >
                  <div className="col s12 m4">
                    <img className="responsive-img" width="100%" src={pol1} alt=""/>
                  </div>
                  <div className="col s12 m8 center-align">
                    <img className="responsive-img" src={pol2} alt=""/>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <ChatQuantum/>
            
            <div className="col s12 m4">
              <Fotones/>
              <Siftting/>
            </div>

            <Receptor/>
        </div>
      </div>
);

export default CardsRow;
