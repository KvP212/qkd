import React, { Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {
    render (){
        return(
            <nav className="indigo darken-4">
              <Link className="brand-logo" to="/">
                <i className="large material-icons white-text">vpn_lock</i>
                Quantum Key Distribution
              </Link>
            </nav>
        )
    }
}

export default Header;
