import React from "react";
import "./footer.css";

const Footer = () => (
    <footer className="page-footer cyan lighten-1">
        <p className="footer-copyright">&copy; Copyright 2019 - KvP212</p>
    </footer>
);

export default Footer;
