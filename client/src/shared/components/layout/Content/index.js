import React from "react";
import "./content.css"

const Content = (props) => (
    <div className="col s12 kvp-color">
        {props.children}
    </div>
);

export default Content;
