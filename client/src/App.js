import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import 'materialize-css/dist/css/materialize.min.css';
import Header from "./shared/components/layout/Header";
import Content from "./shared/components/layout/Content";
import Footer from "./shared/components/layout/Footer";
import { Provider } from "react-redux";
import store from "./store";
import CardsRow from './shared/components/CardsRow';
import SelectionUser from './components/SelectionUser';
import AlgorithmRSA from './components/AlgorithmRSA';
import AlgorithmAES from './components/AlgorithmAES';
import "./app.css"

function App() {
  return (
    <Router>
    <header>
      <Header/>
    </header>
    <main>
      <Provider store={store}>
        <Content>
          {/* components by ./components */}
          <Route path="/" exact component={SelectionUser}/>
          <Route path="/ana" component={CardsRow}/>
          <Route path="/rsa" component={AlgorithmRSA}/>
          <Route path="/aes" component={AlgorithmAES}/>
        </Content>
      </Provider>
    </main>
      <Footer/>
    </Router>
  );
}

export default App;
