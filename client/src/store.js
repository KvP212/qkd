import { createStore, applyMiddleware } from "redux";
import update from "immutability-helper";
import bigInt from "big-integer";
import aesjs from "aes-js";


const initialState = {
  base: "",
  bit: "",
  sendFotones: false,
  sendFotonNumber: -1,
  fotonesA: [],
  keyA: "",
  moveright: false,
  basesA: [],

  fotonesSend: [],
  keyB: "",
  baseB: "",
  basesB: [],

  displayRec: false,
  newKeyA: "",
  newKeyB: "",
  errorDiscrepancia: 0,
  keyDepuradaA: "",
  keyDepuradaB: "",
  keySharedA: "",
  keySharedB: "",

  espiaColor: false,

  primoP: 0,
  primoQ: 0,
  varn: 0,
  varPhi: 0,
  primoE: 0,
  varInv: 0,
  textC:"",
  textCNum: "",
  textD:"",
  textDNum:"",
  arregloE: "",
  testD: 0,
  textCtest: "",

  keyAES: [],
  textCencryp: "",
  textCHexAES: "",
  textDAES: "",

}

const reducerQuantum = (state = initialState, action) => {
  //state: estado actual data en mi app
  //action: que cambio debemos efectuar en el estado
  switch (action.type) {
    case "SAVE_BIT_SELECTED":  
      return{
        ...state,
        bit: action.bit
      }
    
    case "SAVE_BASE_SELECTED":  
      return{
        ...state,
        base: action.base
      }

    case "SAVE_FOTON":  
      if(state.fotonesA.length === 12){
        return state;
      }

      if( state.bit === "0" && state.base === "+") {
        return{
          ...state,
          fotonesA: state.fotonesA.concat({id: action.id, foton: "|0>"}),
          keyA: state.keyA.concat(state.bit),
          basesA: state.basesA.concat({id: action.id, base: "+", color: ""})
        }

      } else if (state.bit === "1" && state.base === "+") {
        return{
          ...state,
          fotonesA: state.fotonesA.concat({id: action.id, foton:"|1>"}),
          keyA: state.keyA.concat(state.bit),
          basesA: state.basesA.concat({id: action.id, base: "+", color: ""})
        }

      } else if (state.bit === "0" && state.base === "x") {
        return{
          ...state,
          fotonesA: state.fotonesA.concat({id: action.id, foton:"|+>"}),
          keyA: state.keyA.concat(state.bit),
          basesA: state.basesA.concat({id: action.id, base: "x", color: ""})
        }
        
      } else if (state.bit === "1" && state.base === "x") {
        return{
          ...state,
          fotonesA: state.fotonesA.concat({id: action.id, foton:"|x>"}),
          keyA: state.keyA.concat(state.bit),
          basesA: state.basesA.concat({id: action.id, base: "x", color: ""})
        }
      }
      return state;
    
    case "SEND_FOTONES":
      var { sendFotonNumber } = state
      if(state.fotonesA.length !== 0 && ++sendFotonNumber !== 12){
        return {
          ...state,
          sendFotones: true,
          sendFotonNumber: sendFotonNumber
        }

      } else{
        return state;
      }

    case "SEND_FOTONES_ALL":
      return {
        ...state,
        fotonesSend: state.fotonesSend.concat(
          state.fotonesA.filter( (foton) => (foton.id > state.sendFotonNumber), {})
        ),
        sendFotonNumber: 11,
      }

    case "FOTON_MOVE":
      return {
        ...state,
        moveright: state.moveright === false ? true: false,
        fotonesSend: state.fotonesSend.concat( state.fotonesA.filter( (foton) => (foton.id === action.id), {}) )
      }
    
    case "FOTON_MOVE_ESPIA":
      var responseFoton = "";
      if( state.fotonesA[action.id].foton === "|0>"
          || state.fotonesA[action.id].foton === "|1>"){
        if(Math.floor(Math.random() * 2) === 0){
          responseFoton = "|+>";
        } else {
          responseFoton = "|x>";
        }

      } else if (state.fotonesA[action.id].foton === "|+>"
        || state.fotonesA[action.id].foton === "|x>"){
        
        if(Math.floor(Math.random() * 2) === 0){
          responseFoton = "|0>";
        } else {
          responseFoton = "|1>";
        }
      }
      return {
          ...state,
          moveright: state.moveright === false ? true: false,
          fotonesSend: state.fotonesSend.concat( {id: action.id, foton: responseFoton} ),
          espiaColor: true,
        }
  
    case "FOTON_RETURN":
      return {
        ...state,
        moveright: false,
        sendFotones: false,
        espiaColor: false,
      }
    
    case "SAVE_BASE_B":  
      return{
        ...state,
        baseB: action.baseB
      }
    
    case "GET_FOTON":
      var found = state.fotonesSend.filter((foton)=>(foton.id === action.id));
      if(found.length === 0){
        return state
      }

      if (state.baseB === "+" && found[0].foton === "|0>") {
        return{
          ...state,
          keyB: state.keyB.concat("0"),
          basesB: state.basesB.concat({id: action.id, base: "+", color: ""})
        }
      }

      if (state.baseB === "+" && found[0].foton === "|1>") {
        return{
          ...state,
          keyB: state.keyB.concat("1"),
          basesB: state.basesB.concat({id: action.id, base: "+", color: ""})
        }
      }

      if (state.baseB === "x" && found[0].foton === "|+>") {
        return{
          ...state,
          keyB: state.keyB.concat("0"),
          basesB: state.basesB.concat({id: action.id, base: "x", color: ""})
        }
      }

      if (state.baseB === "x" && found[0].foton === "|x>") {
        return{
          ...state,
          keyB: state.keyB.concat("1"),
          basesB: state.basesB.concat({id: action.id, base: "x", color: ""})
        }
      }

      if(Math.floor(Math.random() * 2) === 0){
        return{
          ...state,
          keyB: state.keyB.concat("0"),
          basesB: state.basesB.concat({id: action.id, base: state.baseB, color: ""})
        }
      } else {
        return{
          ...state,
          keyB: state.keyB.concat("1"),
          basesB: state.basesB.concat({id: action.id, base: state.baseB, color: ""})
        }
      }
    
    case "SIFTTING":
      var newObj = Object.assign({}, state);

      for (const obj in state.basesA){
        
        if(state.basesA[obj].base === state.basesB[obj].base){
          const newObj2 = update( 
            newObj, 
            {
              basesA: 
                {
                  [obj]: 
                    {
                      color: {$set: "light-green accent-3" }
                    }
                },

              basesB:
                {
                  [obj]: 
                    {
                      color: {$set: "light-green accent-3" }
                    }
                },
              
                newKeyA: {$set: newObj.newKeyA.concat( newObj.keyA.charAt(obj))},
                newKeyB: {$set: newObj.newKeyB.concat( newObj.keyB.charAt(obj))},
            }
          );

          newObj = newObj2;

        } else {
          const newObj2 = update( 
            newObj, 
            {
              basesA: 
                {
                  [obj]: 
                    {
                      color: {$set: "red darken-4 white-text" }
                    }
                },
              
              basesB: 
                {
                  [obj]: 
                    {
                      color: {$set: "red darken-4 white-text" }
                    }
                }
            }
          );

          newObj = newObj2;
          
        }

      }
      
      var errores = 0;
      var longEva = 0;
      var keySharedATemp = "";
      var keySharedBTemp = "";

      for (var i= 0; i < newObj.newKeyA.length; i++){
        if( i % 2 === 0){
          newObj.keyDepuradaA += newObj.newKeyA.charAt(i);
          newObj.keyDepuradaB += newObj.newKeyB.charAt(i);

        } else {
          keySharedATemp += newObj.newKeyA.charAt(i);
          keySharedBTemp += newObj.newKeyB.charAt(i);

          if(newObj.newKeyA.charAt(i) !== newObj.newKeyB.charAt(i)){
            errores++;
          }
          longEva++;
        }
        
      }

      
      return {
        ...state,
        basesA: newObj.basesA,
        basesB: newObj.basesB,
        newKeyA: newObj.newKeyA,
        newKeyB: newObj.newKeyB,
        errorDiscrepancia: (errores / longEva) * 100,
        keyDepuradaA: newObj.keyDepuradaA,
        keyDepuradaB: newObj.keyDepuradaB,
        keySharedA: keySharedATemp,
        keySharedB: keySharedBTemp,
      };
    
    case "SAVE_P":
      if(isNaN(action.value) || action.value
      < 0){
        return state;
      }

      return{
        ...state,
        primoP: action.value,
        varn: action.value * state.primoQ,
        varPhi: (action.value - 1)*(state.primoQ -1),
      };
    
    case "SAVE_Q":
      if(isNaN(action.value) || state.primoP === 0 || action.value
      < 0){
        return state;
      }

      var tempPhi = (state.primoP - 1)*(action.value -1);
      var arregloE = "";
      for(var h= 0; h < tempPhi; h++){
        try {
          if(bigInt(tempPhi).modInv(h).equals(1) === true){
            arregloE += h;
            arregloE += " ";     
          }
        } catch (error) {}
      }

      return{
        ...state,
        primoQ: action.value,
        varn: state.primoP * action.value,
        varPhi: (state.primoP - 1)*(action.value -1),
        arregloE,
      };
    
    case "SAVE_E":
      if(isNaN(action.value) || state.varPhi === 0 || action.value
      < 0){
        return state;
      }

      var resultInv = invmul(state.varPhi, action.value);
      return{
        ...state,
        primoE: action.value,
        varInv: Math.round(resultInv),
      };

    case "SAVE_TEXTC":
      var nnn = "";
      var nnnCifrado = "";      

      for(var j= 0; j < action.value.length; j++){

        nnn += action.value.charCodeAt(j)
        nnn += " ";
        nnnCifrado += powerMod(action.value.charCodeAt(j), state.primoE, state.varn)
        nnnCifrado += " ";
        
      };

      return{
        ...state,
        textC: action.value,
        textCNum: nnn,
        textDNum: nnnCifrado,
        textCtest: action.value,
      }

    case "SAVE_KEY_AES":
      if(action.value === null){
        return state;
      }
      var tempKeyAES = [];

      for(let i in action.value){
        tempKeyAES.push(action.value.charCodeAt(i));
      }

      return {
        ...state,
        keyAES: tempKeyAES,
      };

    case "SAVE_TEXT_TO_BYTES_AES":
        if(action.value === null || state.keyAES.length === 0){
          return state;
        }

        // Convert text to bytes
        var textBytes = aesjs.utils.utf8.toBytes(action.value);

        // The counter is optional, and if omitted will begin at 1
        var aesCtr = new aesjs.ModeOfOperation.ctr(state.keyAES, new aesjs.Counter(5));
        var encryptedBytes = aesCtr.encrypt(textBytes);
        // To print or store the binary data, you may convert it to hex
        var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
        var tempEncrypteHex = "";
        for(let i in encryptedHex){
          tempEncrypteHex += encryptedHex.charAt(i);
          if( i % 2 !== 0){
            tempEncrypteHex += " ";
          }
        }

        return {
          ...state,
          textCHexAES: tempEncrypteHex,
          textCencryp: encryptedHex,
        };
      
      case "SAVE_KEY_DECODE":
        if(state.textCHexAES === "" || action.value.length !== 16){
          return state;
        }

        var tempKeyAESDecode = [];

        for(let i in action.value){
          tempKeyAESDecode.push(action.value.charCodeAt(i));
        }
        console.log(tempKeyAESDecode);

        // When ready to decrypt the hex string, convert it back to bytes
        encryptedBytes = aesjs.utils.hex.toBytes(state.textCencryp);

        // The counter mode of operation maintains internal state, so to
        // decrypt a new instance must be instantiated.
        aesCtr = new aesjs.ModeOfOperation.ctr(tempKeyAESDecode, new aesjs.Counter(5));
        var decryptedBytes = aesCtr.decrypt(encryptedBytes);

        // Convert our bytes back into text
        var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);

        return{
          ...state,
          textDAES: decryptedText,
        }

    case "SAVE_TEST_D":

        if(isNaN(action.value) || state.textCtest === "" || action.value
        < 0){
          return state;
        }
        var aDesifrar = "";
        
        for(var x= 0; x < state.textCtest.length; x++){
          aDesifrar += powerMod(
                          powerMod(state.textCtest.charCodeAt(x), state.primoE, state.varn),
                          action.value,
                          state.varn
                          );
          aDesifrar += " ";
        };
        
        return {
          ...state,
          textD: aDesifrar,
        }

    default:
      return state;
  }
    
}

function invmul(a, b){
	var origA = a;
    var x = 0;
    var prevX = 1;
    var y = 1;
    var prevY = 0;
    while ( b !== 0){
    	var temp = b;
        var quotient = a/b;
        b = a % b;
        a = temp;
        temp = x;
        a = prevX - quotient * x;
        prevX = temp;
        temp = y;
        y = prevY - quotient * y;
        prevY = temp;
    }
    
    return origA + prevY;
}

function powerMod(base, exponent, modulus) {
  if (modulus === 1) return 0;
  var result = 1;
  base = base % modulus;
  while (exponent > 0) {
      if (exponent % 2 === 1)  //odd number
          result = (result * base) % modulus;
      exponent = exponent >> 1; //divide by 2
      base = (base * base) % modulus;
  }
  return result;
}

function logger({ getState }) {
  return next => action => {
    console.log('will dispatch', action)

    // Call the next dispatch method in the middleware chain.
    const returnValue = next(action)

    console.log('state after dispatch', getState())

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue
  }
}

export default createStore(reducerQuantum, applyMiddleware(logger));
