import React from 'react';
import userImage from "../../shared/img/qbits.jpg";
import userImage2 from "../../shared/img/rsa.png";
import userImage3 from "../../shared/img/aes.png";
import { Link } from "react-router-dom";
import "./selectionUser.css";

const SelectionUser = () => {
  return (
    <div className="container">
    <div className="row">
    
    <div className="col s12 m4">
      <div className="card">
        <div className="card-image">
          <img className="user-image-small-2" src={userImage} alt=""/>
          <span className="card-title">Protocolo BB84</span>
        </div>
        
        <div className="card-action">
          <Link className="black-text" to="/ana"><strong>GO</strong></Link>
        </div>
      </div>
    </div>

    <div className="col s12 m4">
      <div className="card">
        <div className="card-image">
          <img className="user-image-small-2" src={userImage3} alt=""/>
          <span className="card-title black-text">Algoritmo AES</span>
        </div>
        
        <div className="card-action">
          <Link className="black-text" to="/aes"><strong>GO</strong></Link>
        </div>
      </div>
    </div>

    <div className="col s12 m4">
      <div className="card">
        <div className="card-image">
          <img className="user-image-small-2" src={userImage2} alt=""/>
          <span className="card-title">Algoritmo RSA</span>
        </div>
        
        <div className="card-action">
          <Link className="black-text" to="/rsa"><strong>GO</strong></Link>
        </div>
      </div>
    </div>

    </div>
    </div>
  );
};

export default SelectionUser;
