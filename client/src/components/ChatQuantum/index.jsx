import React from "react";
import { connect } from "react-redux";

import userImage from "../../shared/img/Nice.png";
import "./chatQuantum.css";

const ChatQuantum = ({base, bit, fotonesA, keyA, sendFotones, sendFotonNumber, basesA, newKeyA, keyDepuradaA, keySharedA, handleSendFotones, handleBit, handleBase, handleFoton, handleSendFotonesAll}) => (
  
  <div className="col s12 m4">
  <div className="card">
    <div className="card-content">
      <div className="valign-wrapper">
        <img className="circle responsive-img user-image-small"  src={userImage} alt=""></img>
        <span className="card-title">1) Emisor</span>
      </div>          

        <p>Bit</p>
      <div className="collection">
        <a 
          href="#!" 
          onClick={() => handleBit("0")}
          className={
            "collection-item col s12 m6 " +
            (bit === "" || bit === "1"? "":"active")}>
          <p className="flow-text">0</p>
        </a>
        <a 
          href="#!" 
          onClick={() => handleBit("1")}
          className={
            "collection-item col s12 m6 " +
            (bit === "" || bit === "0"? "":"active")}>
          <p className="flow-text">1</p>
        </a>
      </div>

        <p>Base</p>
      <div className="collection">
        <a 
          href="#!" 
          onClick={() => handleBase("+")}
          className={
            "collection-item col s12 m6 " + 
            (base === "" || base === "x"? "":"active")}>
          <p className="flow-text">+</p>
        </a>
        <a 
          href="#!" 
          onClick={() => handleBase("x")}
          className={
            "collection-item col s12 m6 " + 
            (base === "" || base === "+" ? "":"active")} >
          <p className="flow-text">x</p>
        </a>
      </div>

      <a 
        href="#!" 
        onClick = {() => handleFoton()}
        className={"waves-effect waves-light btn-small col s3 m4 " + 
        (base === "" || bit === "" || keyA.length === 12 ? "disabled": "")}>
        <i className="material-icons right">cloud</i>
        Add
      </a>
      <br/>
    </div>

    <div className="card-action ">
      <div className="row">
        
        <h6>Key: {keyA}</h6>
        
        <p>Bases:</p>
        <ul className="collection col s12">
          {basesA.map( (base) => {
            return <li key={base.id} className="collection-item col s2 font-small">{base.base}</li>;
          })}
        </ul>
            
        <p>Fotones:</p>
        <ul className="collection col s12">
          {fotonesA.map( (foton) => {
            return <li key={foton.id} className="collection-item col s2 font-small">{foton.foton}</li>;
          })}
        </ul>

        <div className="col s12">
          <button 
            className={"btn btn-small waves-effect waves-light kvp-space-button " +
            (sendFotones === true || sendFotonNumber === 11 || keyA.length !== 12 ? "disabled": "")}
            type="button"
            onClick={() => handleSendFotones()}>
            <i className="material-icons">send</i>
          </button>

          <button 
            className={"btn btn-small waves-effect waves-light kvp-space-button " +
            (sendFotones === true || sendFotonNumber === 11 || keyA.length !== 12 ? "disabled": "")}
            type="button"
            onClick={() => handleSendFotonesAll()}>
            <i className="material-icons left">send</i>
            send all
          </button>
        </div>
        
        <div className="col s12">

          <h6>Key Bruta: {newKeyA}</h6>
          
          <h6 className="orange-text text-darken-4">Key Compartida: { keySharedA }</h6>

          <h6>Key Depurada: { keyDepuradaA }</h6>

        </div>

      </div>
    </div>
  </div>
  </div>
);

const mapStateToProps = state =>({
  base: state.base,
  bit: state.bit,
  fotonesA: state.fotonesA,
  keyA: state.keyA,
  sendFotones: state.sendFotones,
  sendFotonNumber: state.sendFotonNumber,
  basesA: state.basesA,
  newKeyA: state.newKeyA,
  keyDepuradaA: state.keyDepuradaA,
  keySharedA: state.keySharedA,
});

var prevId = 0;

const mapDispatchToProps = dispatch =>({
  handleBit(bit){
    dispatch({
      type: "SAVE_BIT_SELECTED",
      bit
    })
  },

  handleBase(base){
    dispatch({
      type: "SAVE_BASE_SELECTED",
      base
    })
  },

  handleFoton(){
    dispatch({
      type: "SAVE_FOTON",
      id: prevId++
    })
  },

  handleSendFotones(){
    dispatch({
      type: "SEND_FOTONES",
    })
  },

  handleSendFotonesAll(){
    dispatch({
      type: "SEND_FOTONES_ALL",
    })
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(ChatQuantum);
