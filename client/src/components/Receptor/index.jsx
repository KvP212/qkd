import React from "react";
import { connect } from "react-redux";

import userImage from "../../shared/img/Nice.png";

const Receptor = ({baseB, keyB, basesB, newKeyB,  keyDepuradaB, keySharedB, handleFotonGet, handleBase}) => (
  
  <div className="col s12 m4">
  <div className="card">
    <div className="card-content">
      <div className="valign-wrapper">
        <img className="circle responsive-img user-image-small"  src={userImage} alt=""></img>
        <span className="card-title">3) Receptor</span>
      </div>          

        <p>Base</p>
      <div className="collection">
        <a 
          href="#!" 
          onClick={() => handleBase("+")}
          className={
            "collection-item col s12 m6 " + 
            (baseB === "" || baseB === "x"? "":"active")}>
          <p className="flow-text">+</p>
        </a>
        <a 
          href="#!" 
          onClick={() => handleBase("x")}
          className={
            "collection-item col s12 m6 " + 
            (baseB === "" || baseB === "+" ? "":"active")} >
          <p className="flow-text">x</p>
        </a>
      </div>

      <a 
        href="#!" 
        onClick = {() => handleFotonGet()}
        className={"waves-effect waves-light btn-small col s3 m4 " + 
        (baseB === "" ? "disabled": "")}>
        <i className="material-icons right">cloud</i>
        Get
      </a>
      <br/>
    </div>

    <div className="card-action ">
      <div className="row">
        
        <h6>Key: {keyB}</h6>
        
        <p>Bases:</p>
        <ul className="collection col s12">
          {basesB.map( (base) => {
            return <li key={base.id} className="collection-item col s2 font-small">{base.base}</li>;
          })}
        </ul>
        
        <h6>Key Bruta: {newKeyB}</h6>

        <h6 className="orange-text text-darken-4">Key Compartida: { keySharedB }</h6>

        <h6>Key Depurada: { keyDepuradaB }</h6>

      </div>
    </div>
  </div>
  </div>
);

const mapStateToProps = state =>({
  baseB: state.baseB,
  keyB: state.keyB,
  basesB: state.basesB,
  newKeyB: state.newKeyB,
  keyDepuradaB: state.keyDepuradaB,
  keySharedB: state.keySharedB,
});

var prevId = 0;

const mapDispatchToProps = dispatch =>({
  handleFotonGet(){
    dispatch({
      type: "GET_FOTON",
      id: prevId++
    })
  },

  handleBase(baseB){
    dispatch({
      type: "SAVE_BASE_B",
      baseB
    })
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(Receptor);
