import React from "react";
import { connect } from "react-redux";

import userImage from "../../shared/img/Nice.png";
import dPrivadoImg from "../../shared/img/dprivado.png";
import varNImg from "../../shared/img/varN.png";
import varPhiImg from "../../shared/img/varPhi.png";
import codificadoImg from "../../shared/img/codificado.png";
import decodificadoImg from "../../shared/img/decodificado.png";


const AlgoritmoRSA = ({primoP, primoQ, varn, varPhi, primoE, varInv, textC, textCNum, textDNum, textD, arregloE, testD, handleChangeP, handleChangeQ, handleChangeE, handleChangeTextC, handleChangeTestD}) => (
  <div className="container">
    <div className="row ">

      <div className="col s12 m6">
      <div className="card">
        <div className="card-content">
          
          <div className="valign-wrapper">
            <img className="circle responsive-img user-image-small"  src={userImage} alt=""></img>
            <span className="card-title"><p className="flow-text">Algoritmo Rivest Shamir Adleman</p></span>
          </div>

          
          <p>Par de Numeros Primos</p>
          <div className="row">
            <div className="input-field col s6">
              <input placeholder="num" id="p" type="number" className="validate" defaultValue={primoP} onChange={(e)=> handleChangeP(e.target.valueAsNumber)}/>
              <label className="active" htmlFor="p">P</label>
            </div>
            <div className="input-field col s6">
              <input placeholder="num" id="q" type="number" className="validate" defaultValue={primoQ} onChange={(e)=> handleChangeQ(e.target.valueAsNumber)}/>
              <label className="active" htmlFor="q">Q</label>
            </div>  
          </div>

          <div className="row">
            
            <div className="col s12 valign-wrapper">
              <img className="responsive-img col s4" src={varNImg} alt=""></img>
              <h5 className="col s8">= {varn}</h5>
            </div>
            
            <div className="col s12 valign-wrapper">
              <img className="responsive-img col s7" src={varPhiImg} alt=""></img>
              <h5 className="col s5">= {varPhi}</h5>
            </div>
            
          </div>
          <div className="col s12"> 
            <blockquote>
              <h5>e</h5>
              <p>[ {arregloE} ]</p>
            </blockquote>
          </div>

          <div className="input-field col s12">
            <input placeholder="1" id="e" type="number" className="validate" defaultValue={primoE} 
            onChange={(e)=> handleChangeE(e.target.valueAsNumber)}/>
          </div>

          <img className="responsive-img col s12 m5" src={dPrivadoImg} alt=""></img>
          <h5>= {varInv}</h5>
          <br/>
          <hr/>

          <h6><strong>RSA</strong></h6>
          <div className="input-field col s12">
            <input placeholder="Frase para Cifrar" id="textC" type="text" className="validate" defaultValue={textC}
            onChange={(e)=> handleChangeTextC(e.target.value)}/>
          </div>

          <p>Frase en numeros:</p>
          <blockquote>{textCNum}</blockquote>

        </div>

      </div>
      </div>
      
      <div className="col s12 m6">

      <div className="card">
        <div className="card-content">

          <div className="valign-wrapper">
            <i className="medium material-icons left ">lock_outline</i>
            <span className="card-title">
              <p className="flow-text">Encriptado</p>
            </span>
          </div>

          <br/>

          <div className="row">  
            <img 
              className="responsive-img col s5"
              src={codificadoImg} 
              alt="">
            </img>
          </div>

          <h6>
            <strong>Clave Publica:</strong> (e, n) = ( {primoE} , {varn} )
          </h6>
          
          <blockquote> {textDNum} </blockquote>
        
        </div>
      </div>

      <div className="card">
        <div className="card-content">

          <div className="valign-wrapper">
            <i className="medium material-icons left">lock_open</i>
            <span className="card-title">
              <p className="flow-text">Desencriptado</p>
            </span>
          </div>

          <br/>
          
          <div className="row">            
            <div className="col s12">
              <img className="responsive-img col s12 m5" src={decodificadoImg} alt=""></img>
            </div>
          </div>

          <div className="input-field">
            <input placeholder="num" id="testD" type="number" className="validate" defaultValue={testD} onChange={(e)=> handleChangeTestD(e.target.valueAsNumber)}/>
            <label className="active" htmlFor="testD">d</label>
          </div>

          <h6>
            <strong>Clave Privada:</strong> (d, n) = ( {varInv} , {varn} )
          </h6>

          <blockquote>{textD}</blockquote>
          
        </div>
      </div>

      </div>

    </div>
  </div>
);

const mapStateToProps = state =>({
  primoP: state.primoP,
  primoQ: state.primoQ,
  varn: state.varn,
  varPhi: state.varPhi,
  primoE: state.primoE,
  varInv: state.varInv,
  textC: state.textC,
  textCNum: state.textCNum,
  textDNum: state.textDNum,
  textD: state.textD,
  arregloE: state.arregloE,
  testD: state.testD,

});

const mapDispatchToProps = dispatch =>({
  handleChangeP(value){
    dispatch({
      type: "SAVE_P",
      value
    })
  },

  handleChangeQ(value){
    dispatch({
      type: "SAVE_Q",
      value
    })
  },

  handleChangeE(value){
    dispatch({
      type: "SAVE_E",
      value
    })
  },

  handleChangeTextC(value){
    dispatch({
      type: "SAVE_TEXTC",
      value
    })
  },

  handleChangeTestD(value){
    dispatch({
      type: "SAVE_TEST_D",
      value
    })
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(AlgoritmoRSA);
