import React from "react";
import { connect } from "react-redux";

import "./fotones.css";

function EncontrarFoton(props) {

  var found = props.fA.fotonesA.filter( (foton) => (foton.id === props.fA.sendFotonNumber), {});
  console.log(found)
  return found.length === 0 ? []: found[0].foton;
  
}


const Fotones = ({fotonesA, sendFotones, sendFotonNumber, moveright, espiaColor, handleMoveRight, handleMoveRightEspia}) => (
  
  <div className="col s12">
  <div className="card">
    <div className="card-content">

      <div className="valign-wrapper">
        <i className="small material-icons left">adjust</i>
        <span className="card-title">
          <p className="flow-text">2) Enviando Fotones</p>
        </span>
      </div>

      <hr/>
      <div className="section">        
      
        <a 
          id="scale-demo" 
          href="#!" 
          className={"btn-floating btn-small scale-transition " +
          (sendFotones === true ? "scale-in " : "scale-out ") +
          (moveright === true ? "move-right ": "") +
          (espiaColor === true ? "red darken-4 ": "")}>
          <EncontrarFoton fA={{fotonesA, sendFotonNumber}}/>
        </a>

      </div>  
      <hr/>
      
    </div>

    <div className="card-action">
      <div className="row">
        <div className="col s12">
          <a 
            href="#!"
            onClick ={() => handleMoveRightEspia()}
            className={"waves-effect waves-light btn-small kvp-space-button " +
            (sendFotones === true ? "": "disabled")}>
            Espia
          </a>
            
          <a 
            href="#!"
            onClick ={() => handleMoveRight()}
            className={"waves-effect waves-light btn-small kvp-space-button " +
            (sendFotones === true ? "": "disabled")}>
            Ok
          </a>
        </div>
      </div>
      
    </div>
  </div>
  </div>
);

const mapStateToProps = state =>({
  sendFotones: state.sendFotones,
  fotonesA: state.fotonesA,
  sendFotonNumber: state.sendFotonNumber,
  moveright: state.moveright,
  espiaColor: state.espiaColor,
});

var prevId = 0;

const mapDispatchToProps = dispatch =>({
  handleMoveRight(){
    dispatch({
      type: "FOTON_MOVE",
      id: prevId++
    });

    setTimeout(()=>{
      dispatch({
        type: "FOTON_RETURN",
      });
    }, 1000);

  },

  handleMoveRightEspia(){
    dispatch({
      type: "FOTON_MOVE_ESPIA",
      id: prevId++
    });

    setTimeout(()=>{
      dispatch({
        type: "FOTON_RETURN",
      });
    }, 1000);
  }

});

export default connect(mapStateToProps, mapDispatchToProps)(Fotones);
