import React from "react";
import { connect } from "react-redux";

const Fotones = ({basesA, basesB, newKeyB, errorDiscrepancia,  handleRec}) => (
  
  <div className="col s12">
  <div className="card">
    <div className="card-content">

      <div className="valign-wrapper">
        <i className="small material-icons left">playlist_add_check</i>
        <span className="card-title">
          <p className="flow-text"> 4) Siftting</p>
        </span>
      </div>

      

      <div className="card-action">
        <a 
            href="#!"
            onClick={() => handleRec()}
            className={"waves-effect waves-light btn-small col s12 "}>
            Reconciliacion
        </a>
        <br/>
      </div>
      
      <div className="card-action">
        <div className="row">
          <p>Bases de Emisor:</p>
          <ul className="collection col s12">
            {basesA.map( (base) => {
              return <li key={base.id} className={"collection-item col s2 font-small " + base.color}>{base.base}</li>;
            })}
          </ul>

          <p>Bases de Receptor:</p>
          <ul className="collection col s12">
            {basesB.map( (base) => {
              return <li key={base.id} className={"collection-item col s2 font-small " + base.color}>{base.base}</li>;
            })}
          </ul>

          <h6>Bits Key Bruta: {newKeyB.length}</h6>
          
          <h6>Discrepancia &lt; 11%</h6>
          <p>{`Err / Eval =   ` + errorDiscrepancia.toFixed(2) + "%"}</p>
      </div>
      </div>
    </div>
  </div>
  </div>
);

const mapStateToProps = state =>({
  basesA: state.basesA,
  basesB: state.basesB,
  newKeyB: state.newKeyB,
  errorDiscrepancia: state.errorDiscrepancia,
  
});

const mapDispatchToProps = dispatch =>({
  handleRec(){
    dispatch({
      type: "SIFTTING",
    })
  }
  
});

export default connect(mapStateToProps, mapDispatchToProps)(Fotones);
