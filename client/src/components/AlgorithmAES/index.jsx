import React from "react";
import { connect } from "react-redux";

import userImage from "../../shared/img/Nice.png";

const AlgoritmoAES = ({keyAES, textCHexAES, textDAES, 
  handleChangeKey, handleChangeTTB, handleChangeKeyDecode}) => (
  <div className="container">
    <div className="row">

      <div className="col s12 m6">
    
        <div className="card">
          <div className="card-content">
            
            <div className="valign-wrapper">
              <img className="circle responsive-img user-image-small"  src={userImage} alt=""></img>
              <span className="card-title"><p className="flow-text">Advanced Encryption Standard</p></span>
            </div>

            <h6><strong>128-bit Key: </strong> </h6>
            <div className="input-field">
              <input placeholder="key" id="key" type="text" maxLength="16" 
              onChange={(e)=> handleChangeKey(e.target.value)}/>
            </div>

            <blockquote>{"[" + keyAES.join(" , ") + "]"}</blockquote>
            
            <div className="input-field">
              <textarea placeholder="Frase para Cifrar" id="textToBytes" type="text" className="materialize-textarea" onChange={(e)=> handleChangeTTB(e.target.value)}/>
            </div>

          </div>
        </div>
      
        <div className="card">
          <div className="card-content">

            <div className="valign-wrapper">
              <i className="medium material-icons left ">lock_outline</i>
              <span className="card-title"><p className="flow-text">Encriptado</p></span>
            </div>

            <blockquote> {textCHexAES} </blockquote>
              
          </div>
        </div>
      
      </div>

      <div className="col s12 m6">

        <div className="card">
          <div className="card-content">

            <div 
              style={{'position':'relative', 'paddingBottom':'calc(56.25% + 44px)'}}>
              <iframe 
                title="myFrame"
                src='https://gfycat.com/ifr/WeakCornyBedlingtonterrier' 
                frameBorder='0' 
                scrolling='no' 
                width='100%' 
                height='100%' 
                style={{'position':'absolute', 'top':0, 'left':0}} 
                allowFullScreen={true} ></iframe>
            </div>
          
          </div>
        </div>

        <div className="card">
          <div className="card-content">

            <div className="valign-wrapper">
              <i className="medium material-icons left">lock_open</i>
              <span className="card-title"><p className="flow-text">Desencriptado</p></span>
            </div>

            <h6><strong>128-bit Key: </strong></h6>
            <div className="input-field">
              <input placeholder="key" id="keyDecode" type="text" maxLength="16" 
              onChange={(e)=> handleChangeKeyDecode(e.target.value)}/>
            </div>
            
            <blockquote>{textDAES}</blockquote>

          </div>
        </div>
      
      </div>
      
    </div>
  </div>
);

const mapStateToProps = state =>({
  keyAES: state.keyAES,
  textCHexAES: state.textCHexAES,
  textDAES: state.textDAES,

});

const mapDispatchToProps = dispatch =>({
  handleChangeKey(value){
    dispatch({
      type: "SAVE_KEY_AES",
      value
    })
  },

  handleChangeTTB(value){
    dispatch({
      type: "SAVE_TEXT_TO_BYTES_AES",
      value
    })
  },

  handleChangeKeyDecode(value){
    dispatch({
      type: "SAVE_KEY_DECODE",
      value
    })
  }

});

export default connect(mapStateToProps, mapDispatchToProps)(AlgoritmoAES);
