# Título del Proyecto

_Transmision de una Key por canal Cuánticos, Protocolos BB94, Algoritmos AES, Algoritmos RSA_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Nodejs
React
aes-js
big-integer
immutability-helper
materialize-css
Redux
```

## Ejecutando las pruebas ⚙️

_Ejecutar las pruebas automatizadas para este sistema_

```
---
```

## Deployment 📦

```
npm start
```

## Construido con 🛠️

_React js_

* [React](https://es.reactjs.org/) - El framework web usado
* [Nodejs](https://nodejs.org/es/) - El framework web usado

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Kev Pineda** - *Trabajo Inicial* - [KvP212](https://gitlab.com/KvP212)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/KvP212/qkd/-/project_members) quíenes han participado en este proyecto. 
